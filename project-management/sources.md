# Sources

### 04-04
Yesterday
http://de.aliexpress.com/

http://track.aliexpress.com/logisticsdetail.htm?tradeId=8011688310445835

https://gitlab.com/mark-matura/ble-ips-api

https://dbdiagram.io/d/5e887d7d4495b02c3b893e6c

https://gitlab.com/mark-matura/ble-ips-api/-/jobs/499082831

https://gitlab.com/mark-matura/ble-ips-api/-/settings/ci_cd#autodevops-settings

https://laravel.com/docs/7.x/csrf

https://de.aliexpress.com/

https://www.reichelt.com/ch/de/nodemcu-esp32-wifi-und-bluetooth-modul-debo-jt-esp32-p219897.html?CTYPE=0&MWSTFREE=0&PROVID=2788&
wt_gacha=40286554747_165907808785&PROVID=2788&gclid=EAIaIQobChMI7_P3ofHR6AIVCJSyCh1hhQsaEAkYAyABEgJHBPD_BwE&&r=1

https://track.dhlparcel.co.uk/contact/help-with-my-parcel-delivery?consignmentnumber=RV369834170CN

https://laravel.com/docs/7.x/eloquent-mutators

https://gitlab.com/mark-matura/ble-ips-api/-/merge_requests/1

https://azure.microsoft.com/en-us/blog/finding-circular-foreign-key-references/

https://jsoneditoronline.org/

https://jsonlint.com/

https://service.post.ch/ekp-web/ui/list

https://service.post.ch/ekp-web/ui/list/detail/4rEiCIOYvdWZvQJhWv70rBLQFrAVzSLyyW_-NYcXTK3SuH-x-7IHYA

https://gitlab.com/mark-matura/ble-ips-api/-/new/dev?commit_message=Add+LICENSE&file_name=LICENSE

https://gitlab.com/mark-matura/ble-ips-api/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature%2Fbasic-migration-and-seeder-files

https://laravel.com/docs/4.2/schema

https://laravel.com/docs/7.x/validation#form-request-validation

https://laravel.com/docs/7.x/validation#rule-exists

https://laravel.com/docs/7.x/validation#validating-arrays



### 01.04.2020
https://www.debian.org/CD/

https://ubuntu.com/tutorials/tutorial-create-a-usb-stick-on-windows#4-boot-selection-and-partition-scheme

https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent

https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10

https://www.debian.org/doc/manuals/debian-reference/ch05.en.html

https://kb.intermedia.net/article/3187

https://www.reddit.com/r/PHP/comments/2pzpkc/deploy_to_varwww_or_homedeployusername/

https://www.whatsmydns.net/

https://www.debian.org/CD/http-ftp/

https://www.digitalocean.com/community/tutorials/how-to-create-a-new-sudo-enabled-user-on-ubuntu-18-04-quickstart+

https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart

https://www.linuxbabe.com/debian/install-lamp-stack-debian-10-buster

https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mariadb-php-lamp-stack-on-debian-10

https://www.digitalocean.com/community/tutorials/how-to-install-phpmyadmin-from-source-debian-10

https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-10

https://www.digitalocean.com/community/tutorials/how-to-install-the-latest-mysql-on-debian-10

https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-18-04

https://www.guru99.com/mariadb-vs-mysql.html

https://mariadb.com/kb/en/mariadb-coexist-with-mysql/

https://lumen.laravel.com/docs/5.8/database

https://mariadb.org/

https://unix.stackexchange.com/questions/199101/debian-mounting-a-raid-array

https://www.hostingadvice.com/how-to/nginx-vs-apache/

https://askubuntu.com/questions/629470/gpt-vs-mbr-why-not-mbr

https://www.digitalocean.com/community/questions/proper-permissions-for-web-server-s-directory

https://httpd.apache.org/docs/2.0/misc/security_tips.html

https://askubuntu.com/questions/219615/var-www-vs-home-user-public-html

https://wiki.archlinux.org/index.php/Uniform_look_for_Qt_and_GTK_applications

https://whatis.techtarget.com/definition/mount-point

https://www.digitalocean.com/community/tutorials/how-to-install-phpmyadmin-from-source-debian-10



### 03.31
https://www.linuxquestions.org/questions/linux-server-73/using-var-www-vs-srv-vs-home-user-public_html-806589/

https://ubuntuforums.org/showthread.php?t=1424978

https://www.statista.com/statistics/693303/smart-home-consumer-spending-worldwide/

https://www.statista.com/statistics/802690/worldwide-connected-devices-by-access-technology/

https://www.oreilly.com/library/view/getting-started-with/9781491900550/ch01.html


### 28.03
https://www.reddit.com/r/esp32/comments/dbjz96/lilygo_ttgo_tdisplay_issue_uploading_to_the_board/

http://www.lilygo.cn/prod_view.aspx?TypeId=50033&Id=1126&FId=t3:50033:3

https://www.tindie.com/products/ttgo/lilygor-ttgo-t-display/

https://serverfault.com/questions/102569/should-websites-live-in-var-or-usr-according-to-recommended-usage
