# Roadmap
| KW  | Datum | Ziel                                                                                        | Deadline               | Bemerkung              |
| --- | ----- | ------------------------------------------------------------------------------------------- | ---------------------- | ---------------------- |
| 12  | 16.03 | Zentraler Server einrichten, ESP32 bestellen                                                |                        |                        |
| 13  | 23.03 | Über ähnliche Projekte anderer lesen, konkreter Plan aufstellen, eigenes Projekt definieren |                        |                        |
| 14  | 30.03 | Über Umfang des Projekts entscheiden, Minimalziele festlegen, Entwurf des Vertrags          |                        | **Frühlingsferien**    |
| 15  | 06.04 | Überarbeitung des Vertrags, C++ Primer weiterlesen, über ISM Band und Wifi lesen            |                        | **Frühlingsferien**    |
| 16  | 13.04 | ESP32 einrichten, Batterie Konnektoren löten                                                |                        |                        |
| 17  | 20.04 | Erster Test um RSS Werte herauszulesen                                                      |                        |                        |
| 18  | 27.04 | Datenstream prozessieren                                                                    |                        |                        |
| 19  | 04.05 | Datenstream speichern                                                                       |                        |                        |
| 20  | 11.05 | Datenqualität verbessern (Filtering etc.)                                                   |                        |                        |
| 21  | 18.05 | API einrichten                                                                              |                        |                        |
| 22  | 25.05 | Refactoring                                                                                 |                        |                        |
| 23  | 01.06 | Refactoring                                                                                 |                        |                        |
| 24  | 08.06 | Präsentation vorbereiten                                                                    | Abgabe Zwischenprodukt |                        |
| 25  | 15.06 | Zwischenpräsentation 17. Juni, **Refactoring**                                              | Zwischenpräsentation   |                        |
| 26  | 22.06 | Refactoring, real time RSSI corrections, Test RPi 4 model b to see performance impact       |                        |                        |
| 27  | 29.06 | Finish with real time RSSI corrections                                                      |                        |                        |
| 28  | 06.07 | -                                                                                           |                        | **Sommerferien**       |
| 29  | 13.07 | -                                                                                           |                        | **Sommerferien**       |
| 30  | 20.07 | -                                                                                           |                        | **Sommerferien**       |
| 31  | 27.07 | -                                                                                           |                        | **Sommerferien**       |
| 32  | 03.08 | -                                                                                           |                        | **Sommerferien**       |
| 33  | 10.08 | Start with Kalman Filtering                                                                 |                        |                        |
| 34  | 17.08 | Verbesserung von Kalman Filtering                                                           |                        |                        |
| 35  | 24.08 | Finish Kalman Filtering                                                                     |                        |                        |
| 36  | 31.08 | Environment calibration and curve fitting algorithm improvement                             |                        |                        |
| 37  | 07.09 | Trilateration                                                                               |                        |                        |
| 38  | 14.09 | Trilateration                                                                               |                        |                        |
| 39  | 21.09 | GUI or other way to show off the project in real time                                       |                        |                        |
| 40  | 28.09 | GUI, **Refactoring**                                                                        |                        | **Herbstferien**       |
| 41  | 05.10 | Improve IoT and developer ease of use / installation, API documentation                     |                        | **Herbstferien**       |
| 42  | 12.10 | Report / Schw. Jugend forscht?                                                              |                        | **Kompensationswoche** |
| 43  | 19.10 | Report / Schw. Jugend forscht?                                                              |                        |                        |
| 44  | 26.10 | **31. Oct. Schweizer Jugend Forscht DEADLINE**                                              |                        |                        |
| 45  | 02.11 | GUI                                                                                         |                        |                        |
| 46  | 09.11 | GUI                                                                                         |                        |                        |
| 47  | 16.11 | Poster und Abstract                                                                         | Upload des Abstracts   |                        |
| 48  | 23.11 | Abgabe der Maturaarbeit und des Arbeitsjournals                                             | Abgabe Arbeit          |                        |
| 49  | 30.11 | Schlusspräsentation 1. bis 3. Dezember                                                      | Schlusspräsentation    |                        |