# 05-08 Data processing and case printing

Today I didnt encounter many problems, so I will keep this article short. I did some general repository housekeeping and archive the library and esp32 client folder. I also designed 4 more versions of the esp32 case, here is the final version:

![Version 3 with cool hex mesh](./assets/05-08/case.png){ width=60% }

## Next steps
I am on track in terms of internal deadlines. Now it is time to figure out what to do with all of the data that is coming from the ESP32s. In one minute 3050 measurements from 5 ESP32 are fed into the database. That is 610 RSSI values per device or about 10 values per second.

I started googling about what to do with this but it seems quite difficult. Will write more when I know what I am going to do, now it makes most sense to read a bit about sensor fusion and particle filtering.
