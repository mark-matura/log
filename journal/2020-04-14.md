# 04-14 Rewrite functions groups as classes

I spent most of today reading in the C++ Primer to alleviate my troublesome relationship with static typing. I briefly reviewed my code and decided to do a first round of refactoring. I previously tried to encapsulate a lot of logic in functions, but it didnt really work since I ended up using global functions to pass around data. This is really dangerous since there is unrestricted access to morph anything accidentally. I dont want to pass around params like a hot potato and have to copy them around the whole time for performance reasons.

That is why I wrote a console class that should emulate the good old browser console. It provides an interface to the display but encapsulates the private display member variable so no one can mess with it from outside the class. Nice. I havent tried to compile it so far, but I think there will be many issues. That is a challenge for tomorrow though.
