# 04-23 Async HTTPS

Today I made the httpmanager to use an [async tcp library](https://github.com/me-no-dev/AsyncTCP). For plain asynchronous http (no encryption) it works fine.

## Problem
It turns out that the library doesnt support ssl. That is a problem, since my server upgrades all requests to https. The server errors out when it encounters a plain http request on port 80. Some developers are working on a [pull request](https://github.com/me-no-dev/AsyncTCP/pull/48) for TLS support in their free time. They are quite active, the last pull request was 7 days ago. They are not finished yet and the dev release is not stable.

## Solutions

### Make Server accept insecure HTTP on port 80
I can change the apache config to use plain http as well. This would allow the async client to work without me having to wait for tls support. It is a fairly easy fix since I only need to change a few lines in the configuration files. The drawback would be that I would be violating a requirement of my contract in which I stated that connections would be secure.

### MQTT instead of HTTP
I could also switch from async HTTP to async [MQTT](https://github.com/espressif/esp-mqtt). MQTT is more optimized for IoT traffic than HTTP and can be called asynchrously. It appears though that SSL only works with synchrounous MQTT. The async version [uses the same asynctcp library](https://github.com/marvinroger/async-mqtt-client/issues/111) from above that doesnt have ssl support. That would be the least of my problems if I switch to MQTT. The major drawback would be that I would need to redo my entire backend. I would need to set up port forwarding, disable firewall for port 8883 and set up a broker / publisher for the backend and esp32s. I would also have to change the entire backend on the server since Laravel php doesnt support MQTT. There are some [packages for it on packagist](https://packagist.org/packages/salmanzafar/laravel-mqtt) but there is no official support. Having a codebase based on so many libraries also in the backend will be very fragile.

### Go back to sync HTTPS without HTTPClient lib
I can just forget about asynchrounous communication and use the [WifiClientSecure.h](https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFiClientSecure) package from espressif systems directly. I already use it for all of my currenty connections but I havent directly used it to [print HTTP headers to the socket](https://github.com/espressif/arduino-esp32/blob/master/libraries/WiFiClientSecure/examples/WiFiClientSecure/WiFiClientSecure.ino) as shown in this example. This needs no server reconfiguration and should not through `connection refused -1` exceptions like when I used in in conjunction with the HTTPClient. It seemed to be the fault of the HTTPClient, which only provided a wrapper for easily setting request headers and the message body. I think there is potential to make it work without the httpclient.

https://github.com/espressif/arduino-esp32/issues/3449

## Further actions
Both viable solutions have tradeoffs. In the I requirements for the project is for secure connections between clients and api. I think SSL is more or less a necessity and do not think that async traffic is super necessary. As long as the WifiClient is fast, I have no issues with using it synchronously.

Having working code now is more valuable than being promised some better code in the future. I will scrap the async http library and change the server config to not upgrade requests.

