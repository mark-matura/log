# 08-29 Implementing trilateration

Today I did error handling for loading devices and environment path loss coefficients. I also fixed some bugs with calibrating the coefficients.

## Solved Docker Autobuild:
It turns out that the only issue is the autobuild commands I added to the dockerfile. In my eyes it would make more sense if the container build process would do all of the work for assembling the backend, such as migrating database tables and setting up composer. This does not work because docker only mounts the filesystem [AFTER running all commands in the dockerfile](https://stackoverflow.com/questions/46786589/running-composer-install-within-a-dockerfile). The trick is to use the CMD directive, which runs a command post build. The downside is that it is run every single time the container is started, which increases startup time.

Today I also started looking into unit testing python, but it seems hard to mock database insertions and attempted data retrieval. That is something to look at some other day.
