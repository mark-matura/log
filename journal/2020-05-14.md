# 05-14 LSR vs AA evaluation
Here are the results of the log scaled averaging I switched to yesterday. The plotted data consist of averages of 3500 received signal strengths taken over the course of 5 minutes. They combine RSSI values for all five peripherals.

In red are the average RSSI values of measurements taken at 50cm intervals (0.5m, 1m, 1.5m, 2m, etc.) The green line's curvature is based on the result of my log scaled antialiasing algorithm on the server side. The blue line's curvature is calculated using the least squares algorithm in GNUPlot, a statistical tool.

![Logarithmically scaled RSSI](./assets/05-14/combined_log_scaled_rssi.svg){width=75%}


Here is the same chart of a single server for reference. The antialiased function seems less prone to drifting due to the outlier at 4.5m.

![Least squares regression vs Antialiasing](./assets/05-14/start_lsr.svg){width=75%}

## Problem
The constants I feed into the path loss model are inaccurate. At distances greater than 3.5m, the same RSSI value can be associated with very different distances due to the flattening of the graph. Consider the value -65: according to the blue function, the distance is 4m. Yet the green curve suggests that the distance for the same RSSI value is actually 5m. This is a very large difference.
#### Path loss model:
$$p(d) = -10n log(d) + \bar{A_0}$$

Signal power is a function of the distance between the transmitter and receiver. $n$ describes the signal propagation potential of the room the sensors are in. $\bar{A_0}$ is defined as the reference path loss at 1m distance. I currently derive it by averaging 70 RSSI measurements at said distance. The logarithmic nature of the PLM exaggerates errors at higher distances.

My statistical evaluation methods yield different results.

Linear regression

$n = 1.26396,  A_0 = - 57.138798827827$

Anti-aliasing

$n = 1.16541,  A_0 = - 57.138798827827$

## Solutions
In tomorrow's blogpost I outline a more detailed list of solutions that I will use.

### LS Regression in backend
Generally speaking linear regression is more accurate than taking averages. $n$ consistently is closer to the measurements than the anti-aliased versions.

### Smooth measurements with band filters
Since the incoming data is very noisy, a band filter can remove outliers and reduce the measurement error. The problem I see with this is that the RSSI tend to oscillate between two values with $\Delta{RSSI} = 10 dBm$. This is already a very large margin that the band filter might have problems with.

### Smooth measurements with a Kalman Filter
The Kalman filter I was planning on implementing might improve the quality of the RSSI I receive. The problem with calculating a correct $n$ and $A_0$ will still persist nonetheless.
