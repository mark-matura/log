# 04-24 Rewrite HTTP Manager for sync WifiClientSecure

I rewrote the `HttpManager::post()` method to use the WifiClientSecure library directly. It now uses no async libraries or HttpClient wrappers. It just prints the HTTP requests directly to the TCP stream. There were no mentionable problems.
