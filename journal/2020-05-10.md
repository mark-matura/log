# 05-10 Noise reduction

Dealing with RSSI in a room is a more complex undertaking than I thought it would be. I took many different measurements and uploaded the results to [ble-ips-files/measurements](https://gitlab.com/mark-matura/ble-ips-files/-/tree/master/measurements). In a brief overview, it seems that measurements with many servers active have a non-gaussian measurements distribution. Measurements where only one server was active have a smaller standard deviation but are also not very gaussian looking. There seems to be a systematic error at play, since the histograms were reproducibly non-gaussian with same peaks and valley for all servers across 3 separate measurement sessions. The standard deviation is pretty bad, it is roughly 10 percent of the mean.

## Problem: Removing noise from RSSI input
The data I am receiving is really noisy. The power level of the signal (RSSI) in a perfect situation is a function of the distance between the receiver (client) and transmitter (server). Since I did not move the devices while measuring and the RSSI fluctuates wildly, it is clear that the signal is contaminated during transmission and reception.

<!-- Here is the RSSI input stream over 50 measurements of a server 1m away. -->

<!-- ![single server results](./assets/05-10/single_server_rssi.png){width=50%} -->

### Hampel filter
A [hampel filter](https://towardsdatascience.com/outlier-detection-with-hampel-filter-85ddf523c73d) is designed to remove outliers from the input stream. It calculates the batch's mean and then removes values that are beyond than a given threshold past the mean. It is not very computationally expensive and it seems to be more or less effective at weeding out random errors. It should make the curve less blocky but reduces the gain which I dont necessarily want.

### Average Filter
Possibly the most simple version of reducing noise is to use a rolling average filter / antialiasing. It is less computationally expensive as the median since it doesn't have to sort the measurements and achieves more or less the same results. Below are some sample means and medians for one round of measurements.

| Server        | 41:3a | 42:a2  | 45:8e  | 48:2e | 57:02  |
| ------------- | ----- | ------ | ------ | ----- | ------ |
| RSSI (Mean)   | -50.3 | -46.68 | -46.68 | -48.6 | -52.06 |
| RSSI (Median) | -51   | -47    | -47    | -48   | -52    |

The polling rate of the Client is fairly high (10 Hz) and should make a rolling average pretty viable.

### Kalman Filter
This seems to be the most popular version of smoothing out the RSSI on the internet. There is linear and nonlinear Kalman Filtering, the former is for gaussian error distributions. [This paper](https://www.researchgate.net/publication/316501991_An_Improved_BLE_Indoor_Localization_with_Kalman-Based_Fusion_An_Experimental_Study) claims that the distribution is log normal and therefore the Kalman filter is applicable. Others argue that a extended filter for non linear systems is necessary for bluetooth.

A kalman filter is a quite sophisticated way of removing noise. It takes a weighted average of a prediction of a state variable and then what was actually measured. Like that it combines the past (prediction) with the present (update by incoming measurement) and can base decisions on numbers with higher certainty. It gives values that aren't trustworthy (outliers) a low weight and smooths the curve.

Since it seems so popular, there also are many BLE specific kalman filtering websites such as [this one ](https://www.wouterbulten.nl/blog/tech/lightweight-javascript-library-for-noise-filtering/) and even whole [implementations](https://github.com/neXenio/BLE-Indoor-Positioning/blob/master/BLE%20Indoor%20Positioning/src/main/java/com/nexenio/bleindoorpositioning/ble/beacon/signal/KalmanFilter.java) online. There are also a plethora of very nice research papers, many of which I had previously read and analyzed before (See documentation section).

\newpage

### Low / high pass filter

With a low pass filter one removes the values below a given cutoff and emphasizes those with higher frequencies. In my case I think it makes sense in any case to restrict the domain to numbers between -30 and -80 dBm. Beyond that the numbers are not trustworthy enough.

### Sensor fusion
Many research papers introduce IMU data for dead reckoning or signal fingerprinting methods. These improve the inaccuracies of the Multilateration method by introducing other sensory inputs. When combined in the Kalman Filter's second update step, the IMU data can be compared with the RSSI prediction and reduce the RSME by a large margin. I am not a huge fan of this though, since my goal is to make BLE IPS more accessible to everyone. Requiring people to buy a IMU module reduces accessibility of my code. A phone has all the sensors needed, but I would then have to switch platforms to [React Native](https://github.com/react-native-sensors/react-native-sensors) and that is not within the scope of the Zwischenpräsentation.

### Back propagation Neural Network
[This paper](https://www.researchgate.net/publication/327256074_Indoor_Positioning_Algorithm_Based_on_the_Improved_RSSI_Distance_Model) suggests using a particle swarm optimized back propagation neural network. The gist is that it finds the optimal parameters for the log distance path loss model. Otherwise this model uses constants to describe shadowing and fading, but these obviously are subject to change when the environment is modified. The BPNN reduces the error. This is a nice idea and I will keep it in the back of my head for after summer vacation.

## Further actions
What seems to make the most sense to me is to implement a kalman filter that refines the RSSI. I will identify the parameters for the path loss model of my living room and calculate these. I already took measurments for many/single servers at different distances and will write more about the results tomorrow. The update phase of the kalman filter will probably be fed with antialiased RSSI since it seems to be the easiest option out there. For the time being I will just use vanilla PHP and Laravel Collections.

