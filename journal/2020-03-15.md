# 03-15 Battery research

## Progress update
Reasearch of possible solutions for a ESP32 that suits my needs. I ordered 10 TTGO-T Dislay ESP32 and am searching for compatible batteries.

Github page of my selected ESP32 board:

[https://github.com/Xinyuan-LilyGO/TTGO-T-Display](https://github.com/Xinyuan-LilyGO/TTGO-T-Display)

Product info page from manufacturer

[http://www.lilygo.cn/prod_view.aspx?TypeId=50033&Id=1126&FId=t3:50033:3](http://www.lilygo.cn/prod_view.aspx?TypeId=50033&Id=1126&FId=t3:50033:3)

Setup guide for the ESP32 TTGO-T Display

[https://sites.google.com/site/jmaathuis/arduino/lilygo-ttgo-t-display-esp32?authuser=0](https://sites.google.com/site/jmaathuis/arduino/lilygo-ttgo-t-display-esp32?authuser=0)

LiPo battery advice:

- [https://www.reddit.com/r/esp32/comments/fh3ww3/ttgo_tdisplay_battery_connection_lipo_suggestions/?utm_source=amp&utm_medium=&utm_content=tp_vote](https://www.reddit.com/r/esp32/comments/fh3ww3/ttgo_tdisplay_battery_connection_lipo_suggestions/?utm_source=amp&utm_medium=&utm_content=tp_vote)
- [https://github.com/Xinyuan-LilyGO/TTGO-T-Display/issues/9](https://github.com/Xinyuan-LilyGO/TTGO-T-Display/issues/9)
- [https://www.reddit.com/r/esp32/comments/exf9dt/ttgo_tdisplay_findings/](https://www.reddit.com/r/esp32/comments/exf9dt/ttgo_tdisplay_findings/)

## Problems
I am not sure what sort of batteries is compatible with the board. The input voltage is roughly 3.7V, so I am guessing I can buy anything like
[this](https://www.aliexpress.com/item/33003119110.html?spm=a2g0o.productlist.0.0.42e91685wYwmqD&algo_pvid=0e11b6ab-63dd-49ac-b3f2-aa2430783794&algo_expid=0e11b6ab-63dd-49ac-b3f2-aa2430783794-1&btsid=0ab6f82115843064815055779e5413&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)
and then solder the 1.25 mm 2 pin JST connector that is included in the box to the battery. I do not know if the LiPo battery will charge automatically when the ESP32 is plugged in to AC power using the USB-C plug.

## Solutions
Contact someone that knows about the issue and read some more online.

## Further actions
I have written to someone on r/esp32 on reddit who made one of the posts above about the TTGO-T Display ESP32, asking about the possibility to charge the ESP32's batteries directly.
