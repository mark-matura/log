# 09-13 GUI brainstorming

In order to demonstrate the software to a layman, I am considering emulating the concrete situation of a shop. Like this I can show the benefits of indoor positioning systems for consumers and for business owners with a concrete example. For the business side, there are many large benefits: One can tap into information about what customers look at for how long, how they walk through the store and collect data on what they are interested in.

## Problem: Consumer side application
I can't think of a unique application for the consumer side that is only possible using indoor positioning. The first problem is that most ideas that come to mind are mainly beneficial for the business owner (unless you consider getting manipulated by a retail chain a benefit as a customer). The second issue is that the demo has to be based heavily on IPS technology. I have been considering some personalized ads based on what shelf the consumer is standing in front of. But this can be achieved using a basic screen that is set up in front of a shelf advertising the product directly. There are more definitely more interesting IPS applications than that.

## Solutions

### Push Notification with discount
In front of most shelves there is a screen. When the user walks through the store, the app saves what shelves the user looked at closely (slowing down, stopping). Afterwards, the app can target ads towards the products on the shelves for which the user showed increased interest. This can be done with push notifications.

### Send email based on what the customer looked at
Since IPS grants the store owner the knowledge of hwo long someone was looking at something in the store, one could send a follow up email with discounts for customers that left empty handed. For instance if someone came in, looked at a tablet for a while and then left without purchasing it, sending some ads for discounts on tablets might please the customer. It possibly feels like the store cares about it on a more personal level.

## Further actions
I think I will go ahead with the idea of tracking how much time the user spends in front of a shelf and then showing them some ads based on that. What I also would like to include in the GUI is some sort of map view for the shop owner and an easier way to tweak the path loss parameters or update the settings. Another must have is a map that can show the real time or past positions of devices.
