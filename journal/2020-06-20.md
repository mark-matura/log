# 06-20 RT correction and BLE advertising settings

Yesterday I finished the moving averaging algorithm and refactored my work. Today I evaluated the results of the real time client correction algorithm based on antialiased values.

![Real time correction based on antialiased values at 150cm](./assets/06-20/rtcorrectedrssibasedonmvavg150cm.svg)

![Real time correction based on antialiased values at 400cm](./assets/06-20/rtcorrectedrssibasedonmvavg400cm.svg)

The above graphs are based on a stationary client and gateway, that were as close to each other as possible. For this reason, the results of the moving averages (blue and red lines) based on raw measurements (blue and red dots) should have been the same. This does not seem to be the case for the most part. The expected results are constant lines at a certain Y-value. Inducing higher variance by moving the devices 4m apart seemed to reduce the fluctuations in the real time corrections.

The bottom graph has an interesting section towards the left side, where client and gateway's curves were congruent. As a result, the green correction was almost perfectly flat, which shows how the algorithm is capable of smoothing out fluctuations like this.

![Mobile client with recommended window size](./assets/06-20/rtcorrectedmobileclientrecommendedwindow.svg)

![Mobile client with smaller window size](./assets/06-20/rtcorrectedmobileclientsmallwindow.svg)

In a second experiment the setup was tested in a more realistic use case. The client was put on a skateboard and moved away from the BLE peripheral and then moved back to roughly the same position. The gateway was stationary and farther away than the client initially. I conducted two separate runs. The difference between top and below were the windows size of the moving average in the backend. The top uses a window size of 10 measurements, below only 6 measurements are used.

The result is a fairly constant function for the gateway and a parabolic function for the client. The clustering of the red dots around the average is quite good at close distances. Once the client moves away around the middle of the graph, the variance increases greatly and the red line has greater fluctuations.

To interpret the results, there doesn't seem to be a any gain of using a smaller averaging window size. Moreover the lines seem to be more highly fluctuating. In the grand scope of things, my real time corrections do not seem to have a large influence on the results, since the lines are pretty good already due to averaging.

## Problem
Testing coverage of my system is done using a advertising configuration that is atypical for common use cases. This is because my system is designed for very high performance and resolution and does not care about the tradeoff with bad beacon battery life. The results of my high performance real time correction algorithm do not really show a big difference by the correction algorithm.

[The paper](https://blog.markmarolf.com/sources/05-10_sensors-18-02820.pdf) that I used as a reference for my algorithm for instance uses a completely different BLE peripheral-central configuration, which admittedly is more true to the concept of BLE being a low maintenance, low cost IPS solution. Most business BLE beacons use coin cell batteries (CR2032) and have to last for many months on a single charge. My system uses up most of its 2000 mAh within 8 hours. I get very high resolutions of movements (skateboard example above) but battery life suffers.

>We placed four BLE beacons at the same height ... and then placed the Bluetooth gateway at a fixed location ... The transmission interval of the beacon frame of all of the four Bluetooth nodes was set to 500 ms, and 50 times of RSSI were collected continuously.

## Solution
Since I intend for people to actually use my software, I should be testing situations where there is an industry typical advertising situation.

### Advertising intervals
[Estimote](https://community.estimote.com/hc/en-us/articles/201636913-What-are-Broadcasting-Power-RSSI-and-other-characteristics-of-a-beacon-s-signal-), a market leader in the BLE IPS industry suggests that shorter advertising intervals yield more stable signals and power usage decreases linearly with intervals shortening. I use the defaults of the BLE GAP API from the arduino-esp32 BLE library. These use advertising intervals of [20  to 40ms](https://github.com/espressif/arduino-esp32/blob/master/libraries/BLE/src/BLEAdvertising.cpp#L46). This is overkill as shown by an image accompanying [this article](https://support.kontakt.io/hc/en-gb/articles/201567802-Advertising-Interval-best-practise) of kontakt.io.

![interval vs path](./assets/06-20/interval_post_path.png){ width=60% }

An interval of 100ms or even 200ms is probably more than enough and will increase battery life by a factor of 5 to 10 times. It should also be more representative of a not so perfect testing environment and perhaps make my correction algorithm more useful.

### TX power
Higher TX powers drain battery faster but provide more stable signals and allow for longer application distances. The ESP32 API defaults to 3 dBm, which is not very representative of factory defaults of many BLE beacons. [Estimote sets default tx power to -4dBm.](https://community.estimote.com/hc/en-us/articles/204100806-What-are-the-default-settings-of-Estimote-Beacons-How-to-edit-settings-), which is a whole lot lower. By reducing the TX power level, signal stability is also decreased.

### Further action
After spending the last few months of my life trying very hard to reduce noise in RSSI, I now find myself intentionally muddying my results. I think that in the grand scheme of things, it makes sense to try to make my software appealing to as many end users as possible by adapting to a realistic testing environment. If my noise reduction attempts are unsuccessful, I can always revert back to this code.
