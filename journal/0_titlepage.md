---
title: "A Bluetooth Low Energy-Based Indoor Positioning System"
subtitle: "Documentation of Problem Solving and Work Process"
author: "Mark Marolf"
institute: "Kantonsschule Baden"
date: "2020-11-24"
subject: "Projektunterricht"
keywords: [BLE, Indoor Positioning System, RSSI, Raspberry Pi]
papersize: a4
geometry: margin=2cm
version: 1.0
lang: "en"
fontfamily: mathpazo
linkcolor: blue
---