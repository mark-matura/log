# 10-30 Finish report

Today I did the finishing touches on my report and turned it in to SJF. No problems. Next I will finish working on the GUI use-case visualization. As soon as that works, I think I will be pretty much done with this project apart from stuff like posters and writing abstracts. It feels kind of weird to keep on adding things, since writing a conclusive report has been telling my brain that this project is almost over.
