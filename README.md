# BLE Indoor Positioning

![outdoor](https://blog.markmarolf.com/img/R0030124.png)
![client](https://blog.markmarolf.com/img/client.jpg)
![multiclient](https://blog.markmarolf.com/img/multiclient.png)
![setup](https://blog.markmarolf.com/img/setup.jpg)
![beacons](https://blog.markmarolf.com/img/beacons.png)
![outdoor](https://blog.markmarolf.com/img/R0030115.png)

Netflify build status: <br/>
[![Netlify Status](https://api.netlify.com/api/v1/badges/e0a6e2ff-8d80-4bf8-8f81-54993b7a854b/deploy-status)](https://app.netlify.com/sites/keen-wiles-f01e7a/deploys)