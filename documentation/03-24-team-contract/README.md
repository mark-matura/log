# Team Contract

<!-- #### *Short Side Note
I am writing this document in english because I do not consider this sort of documentation to be integral to the project's outcome. This page should be considered a informal and quick review of team contracts. I will solely list points down below that are missing from my contract and highlight the necessity for some selected ones in the conclusion. -->

## MIT Software Construction: Team Contract
- [Link](https://web.mit.edu/6.005/www/fa15/projects/abcplayer/team-contract/)

### Goals
- What are your personal goals for this assignment?
- What kind of obstacles might you encounter in reaching your goals?

### Meeting norms
- How will you use the in-class time?

### Work norms
- How will deadlines be set?
- Where will you record who is responsible for which tasks?

## University of Waterloo: Making Group Contracts
- [Link](https://uwaterloo.ca/centre-for-teaching-excellence/teaching-resources/teaching-tips/developing-assignments/group-work/making-group-contracts)

Expectations (ground rules) regarding preparation for and attendance at group meetings, frequency and duration of meetings, and communication. The contract should focus on behaviours that will be expected of all group members and should only include those behaviours that are crucial to the group's effectiveness.  Groups could aim for five-seven ground rules.

How do you want students to divide the workload? If students choose to "divide and conquer" the work, will they achieve your intended learning outcomes?

Which guidelines, course expectations, or rules are firm and need to be in place before students draft their contracts, and which concepts, issues, and decisions would they benefit from working through as they discuss and create their contracts?
